#!/bin/bash
HDFCI=$(hadoop jar HDFC-1.0-SNAPSHOT.jar HDFC.HDFCistatus hdfs://172.26.5.25:8020/tmp/logging/ '\w+=\d+/\w+=\d+')
REQUEST_PROC=""
PARTITIONS=""
STATEMENT="where "
LIST=$(echo "$HDFCI" | awk '{for (i=2; i<=NF; i++) print $i}' ORS=' ')
for k in $LIST; do
	PARTITIONS="$PARTITIONS PARTITION ($(echo $k | sed 's@/@,@'))"
	REQUEST_PROC="$REQUEST_PROC INSERT OVERWRITE TABLE flume_data_proc PARTITION ($(echo $k | sed 's@/@,@')) SELECT val from flume_data where $(echo $k | sed 's@/@ and @');"
done
echo $(hive -e "ALTER TABLE flume_data ADD IF NOT EXISTS $PARTITIONS;") > /tmp/inotify.log
echo $(hive -e "set hive.exec.dynamic.partition.mode=nonstrict; set hive.exec.dynamic.partition=true; $REQUEST_PROC") >> /tmp/inotify.log
echo $HDFCI >> /tmp/inotify.log
echo $PARTITIONS >> /tmp/inotify.log
echo $REQUEST_PROC >> /tmp/inotify.log
#echo "hadoop jar HDFC-1.0.SNAPSHOT.jar HDFC.HDFCiupdate hdfs://172.26.5.25:8020/tmp/logging/ $(echo "$HDFCI" | awk '{print $1}')" >> /tmp/inotify.log
echo $(hadoop jar HDFC-1.0-SNAPSHOT.jar HDFC.HDFCiupdate hdfs://172.26.5.25:8020/tmp/logging/ $(echo "$HDFCI" | awk '{print $1}')) > /tmp/inotify.log
