#!/bin/bash
HDFCI=$(hadoop jar HDFC-1.0-SNAPSHOT.jar HDFC.HDFCistatus hdfs://172.26.5.25:8020/tmp/logging_processed/ '\w+=\d+/\w+=\d+')
echo $HDFCI > /tmp/inotify.log

REQUEST_PROCH="set hive.exec.dynamic.partition.mode=nonstrict; set hive.exec.dynamic.partition=true; add jar hive-udf-level-1.0-SNAPSHOT.jar; create temporary function level as 'HiveUdfLevel';"

REQUEST_PROCI="connect localhost; drop function if exists level(string); create function level(string)
returns int location '/user/cloudera-scm/hive-udf-level-1.0-SNAPSHOT.jar' symbol='HiveUdfLevel'; invalidate metadata;"

REQUEST_PROCS=""

PARTITIONS="ALTER TABLE flume_data ADD IF NOT EXISTS "

export PYTHON_EGG_CACHE="/tmp/.python-eggs"
STATEMENT="where "
LIST=$(echo "$HDFCI" | awk '{for (i=2; i<=NF; i++) print $i}' ORS=' ')
for k in $LIST; do
	PARTITIONS="$PARTITIONS PARTITION ($(echo $k | sed 's@/@,@'))"
	REQUEST_PROCH="$REQUEST_PROCH INSERT OVERWRITE TABLE flume_data_proc_uh PARTITION ($(echo $k | sed 's@/@,@')) SELECT level(val), val from flume_data_proc where $(echo $k | sed 's@/@ and @');"
	REQUEST_PROCI="$REQUEST_PROCI INSERT OVERWRITE TABLE flume_data_proc_ui PARTITION ($(echo $k | sed 's@/@,@')) SELECT level(val), val from flume_data_proc where $(echo $k | sed 's@/@ and @');"
	REQUEST_PROCS="$REQUEST_PROCS INSERT OVERWRITE TABLE flume_data_proc_spark PARTITION ($(echo $k | sed 's@/@,@')) SELECT val from flume_data_proc where $(echo $k | sed 's@/@ and @');"
done
echo $(hive -e "$PARTITIONS;") >> /tmp/inotify.log
echo $(hive -e "$REQUEST_PROCH") >> /tmp/inotify.log
impala-shell -q "$REQUEST_PROCI" >> /tmp/inotify.log 2>&1
spark-submit --executor-cores 1 --num-executors 1 --driver-memory 768m --executor-memory 768m --class spark.sql --master yarn-cluster --files /etc/hive/conf.cloudera.hive/hive-site.xml,hive-udf-level-1.0-SNAPSHOT.jar SQL-1.0-SNAPSHOT.jar "$REQUEST_PROCS" > /tmp/spark_test.log 2>&1
echo $HDFCI >> /tmp/inotify.log
echo $PARTITIONS >> /tmp/inotify.log
echo $REQUEST_PROCH >> /tmp/inotify.log
echo $REQUEST_PROCI >> /tmp/inotify.log
echo $REQUEST_PROCS >> /tmp/spark_test.log
#echo "hadoop jar HDFC-1.0.SNAPSHOT.jar HDFC.HDFCiupdate hdfs://172.26.5.25:8020/tmp/logging/ $(echo "$HDFCI" | awk '{print $1}')" >> /tmp/inotify.log
echo $(hadoop jar HDFC-1.0-SNAPSHOT.jar HDFC.HDFCiupdate hdfs://172.26.5.25:8020/tmp/logging_processed/ $(echo "$HDFCI" | awk '{print $1}')) >> /tmp/inotify.log
