#!/bin/bash
DEPLOY_DIR="user/cloudera-scm"
OOZIE_SRV=http://bdt-mponomarev-3d941ce2-6666-46c5-bc1e-fbb9cb11e2ed.cop-bigdata-management.c4gd-orion.griddynamics.net:11000/oozie
NAME_NODE=hdfs://bdt-mponomarev-3d941ce2-6666-46c5-bc1e-fbb9cb11e2ed.cop-bigdata-management.c4gd-orion.griddynamics.net:8020
JOB_TRACKER=bdt-mponomarev-3d941ce2-6666-46c5-bc1e-fbb9cb11e2ed.cop-bigdata-management.c4gd-orion.griddynamics.net:8032

git clone https://gitlab.com/devMauris/hive_loglevel.git
git clone https://gitlab.com/devMauris/spark_sql_hive.git
git clone https://gitlab.com/devMauris/HDFCi.git

cd HDFCi && mvn clean install && hadoop fs -rm -f /"$DEPLOY_DIR"/HDFC-1.0-SNAPSHOT.jar &&hadoop fs -put ./target/HDFC-1.0-SNAPSHOT.jar /"$DEPLOY_DIR"/ && cd ..
cd spark_sql_hive && mvn clean install && hadoop fs -rm -f /"$DEPLOY_DIR"/SQL-1.0-SNAPSHOT.jar &&hadoop fs -put ./target/SQL-1.0-SNAPSHOT.jar /"$DEPLOY_DIR"/ && cd ..
cd hive_loglevel && mvn clean install && hadoop fs -rm -f /"$DEPLOY_DIR"/hive-udf-level-1.0-SNAPSHOT.jar &&hadoop fs -put ./target/hive-udf-level-1.0-SNAPSHOT.jar /"$DEPLOY_DIR"/ && cd ..
hadoop fs -rm -f /"$DEPLOY_DIR"/flume_data_proc_update.sh && hadoop fs -put ./flume_data_proc_update.sh /"$DEPLOY_DIR"/ && hadoop fs -rm -f /"$DEPLOY_DIR"/flume_data_update.sh && hadoop fs -put ./flume_data_update.sh /"$DEPLOY_DIR"/
rm -rf hive_loglevel spark_sql_hive HDFCi
hadoop fs -rm -r -f /"$DEPLOY_DIR"/ooziewf && hadoop fs -mkdir -p /"$DEPLOY_DIR"/ooziewf/flume_data_coord/lib && hadoop fs -mkdir -p /"$DEPLOY_DIR"/ooziewf/flume_data/lib && hadoop fs -mkdir -p /"$DEPLOY_DIR"/ooziewf/flume_data_proc/lib && hadoop fs -mkdir -p /"$DEPLOY_DIR"/ooziewf/flume_data_proc_coord/lib

cat ./oozie/flume_data_update.properties | sed "s@DEPLOY_DIR@$DEPLOY_DIR@" | sed "s@NAME_NODE@$NAME_NODE@" | sed "s@JOB_TRACKER@$JOB_TRACKER@"  > job.properties
cat ./oozie/flume_data_update.xml | sed "s@DEPLOY_DIR@/$DEPLOY_DIR@" > workflow.xml
hadoop fs -put ./workflow.xml /"$DEPLOY_DIR"/ooziewf/flume_data/

cat ./oozie/flume_data_update_coord.properties | sed "s@DEPLOY_DIR@$DEPLOY_DIR@" | sed "s@NAME_NODE@$NAME_NODE@" | sed "s@JOB_TRACKER@$JOB_TRACKER@"  > job.properties
cat ./oozie/flume_data_update_coord.xml | sed "s@APP_PATH@/$DEPLOY_DIR/ooziewf/flume_data/workflow.xml@" > coord.xml
hadoop fs -put ./coord.xml /"$DEPLOY_DIR"/ooziewf/flume_data_coord/
oozie job -oozie $OOZIE_SRV -config ./job.properties -run

cat ./oozie/flume_data_update_proc.properties | sed "s@DEPLOY_DIR@$DEPLOY_DIR@" | sed "s@NAME_NODE@$NAME_NODE@" | sed "s@JOB_TRACKER@$JOB_TRACKER@"  > job.properties
cat ./oozie/flume_data_update_proc.xml | sed "s@DEPLOY_DIR@/$DEPLOY_DIR@" > workflow.xml
hadoop fs -put ./workflow.xml /"$DEPLOY_DIR"/ooziewf/flume_data_proc/

cat ./oozie/flume_data_update_proc_coord.properties | sed "s@DEPLOY_DIR@$DEPLOY_DIR@" | sed "s@NAME_NODE@$NAME_NODE@" | sed "s@JOB_TRACKER@$JOB_TRACKER@"  > job.properties
cat ./oozie/flume_data_update_proc_coord.xml | sed "s@APP_PATH@/$DEPLOY_DIR/ooziewf/flume_data_proc/workflow.xml@" > coord.xml
hadoop fs -put ./coord.xml /"$DEPLOY_DIR"/ooziewf/flume_data_proc_coord/
oozie job -oozie $OOZIE_SRV -config ./job.properties -run
